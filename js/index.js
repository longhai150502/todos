
const BASE_URL = "https://635f4b62ca0fe3c21a992480.mockapi.io";

var idEdit = null;

function fetchAllTodo(){
    turnOnLoading();
    axios({
        url: `${BASE_URL}/todos`,
        method: "GET",
    })
        .then(function(res){
            turnOffLoading();
            renderTodoList(res.data);
        })
        .catch(function(err){
            turnOffLoading();
            console.log('err', err)
        });
}
fetchAllTodo();



function removeTodo(idTodo){
    turnOnLoading();
    axios({
        url: `${BASE_URL}/todos/${idTodo}`,
        method: 'DELETE',
    })
        .then(function(res){
            turnOffLoading();
            fetchAllTodo();
            console.log('res', res);
        })
        .catch(function(err){
            turnOffLoading();
            console.log('err', err)
        })
}

function addTodo(){
    var data = layThongTinTuForm()

    var newTodo = {
        name: data.name,
        desc: data.desc,
        isComplete: true,
    };
    turnOnLoading();
    axios({
        url: `${BASE_URL}/todos`,
        method: "POST",
        data: newTodo,
    })
        .then(function(res){
            turnOffLoading();
            fetchAllTodo();
            console.log('res', res);
        })
        .catch(function(err){
            turnOffLoading();
            console.log('err', err)
        })
}

function editTodo(idTodo){
    turnOnLoading();
    axios ({
        url: `${BASE_URL}/todos/${idTodo}`,
        method: "GET",
    }).then(function(res){
        turnOffLoading();
        document.getElementById("name").value = res.data.name;
        document.getElementById("desc").value = res.data.desc;
        idEdit = res.data.id;
    })
    .catch(function(err){
        turnOffLoading();
        console.log('err', err)
    })
}

function updateTodo(){
    turnOnLoading();
    let data = layThongTinTuForm()
    axios ({
        url: `${BASE_URL}/todos/${idEdit}`,
        method: "PUT",
        data: data,
    })
        .then((res) => {
            fetchAllTodo();
            turnOffLoading();
        }).catch((err) => {
            turnOffLoading();
        });
}